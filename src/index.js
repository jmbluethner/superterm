/**
 * @author jmbluethner <mail@jmbluethner.de>
 * @license MIT
 */

const low = require('lowdb');
const FileSync = require('lowdb/adapters/FileSync');
const { app, BrowserWindow } = require('electron');
const fs = require('fs');

/**
 * Initialize Database
 */

const adapter = new FileSync('./db/db.json');
const db = low(adapter);

// Set the default Data blocks if they don't exist already
db.defaults({ connections: [] }).write();

/**
 * Create window & render index
 */

function createWindow () {
    const win = new BrowserWindow({
        width: 800,
        height: 600,
        webPreferences: {
            nodeIntegration: true
        }
    })

    // Hide default Menu bar
    //win.setMenu(null);

    win.loadFile('index.html');
}

app.whenReady().then(createWindow);

app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
        app.quit()
    }
});

app.on('activate', () => {
    if (BrowserWindow.getAllWindows().length === 0) {
        createWindow()
    }
});