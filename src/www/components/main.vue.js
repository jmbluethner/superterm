/**
 * @author Jan-Morris Blüthner <bluethner@heliophobix.com>
 */

const Vue = require('vue');
const low = require('lowdb');
const FileSync = require('lowdb/adapters/FileSync');
const adapter = new FileSync('./db/db.json');
const db = low(adapter);

function createTestDataset(name) {
    const connection = {
        id: 1,
        host: 'ssh.local.host',
        username: 'root',
        post: 22,
        pass: 1970,
        name: name
    }
    db.get('connections').push(connection).write();
}

var bus = {};
bus.eventBus = new Vue();

var app = new Vue({
    el: '#app',
    data: {},

    methods: {},
    created: function () {

    },
    components: {
        'sshmainview': {
            data: function () {
                return {
                    storedSshConnections: [],
                    showNewConnectionWindow: false
                }
            },
            methods:  {
                getStoredSshConnections: function () {
                    let vue = this;
                    vue.storedSshConnections = db.get('connections').value();
                    console.log(vue.storedSshConnections);
                },
                toggleNewConnectionOverlay: function () {
                    this.showNewConnectionWindow = !this.showNewConnectionWindow;
                    console.log('LOL!');
                }
            },
            created: function() {
                this.getStoredSshConnections();
            },
            template:
                '<div class="sidebar">' +
                '   <div class="connections_container">' +
                '       <div v-for="connection in storedSshConnections" data-role="button" class="host_entry">' +
                '           <div class="host_status">' +
                '               <i class="fas fa-terminal up"></i>' +
                '           </div>' +
                '           <div class="host_meta">' +
                '               <span class="connection_name">{{connection.name}}</span>' +
                '               <span class="connection_host">{{connection.host}}</span>' +
                '           </div>' +
                '       </div>' +
                '   </div>' +
                '   <div v-on:click="toggleNewConnectionOverlay" class="add_connection" data-role="button">' +
                '       <span><i class="fas fa-plus"></i>New Connection</span>' +
                '   </div>' +
                '</div>' +
                '<div class="mainview">' +
                '   ' +
                '</div>' +
                '<div v-if="showNewConnectionWindow" class="new_connection_overlay">' +
                '   <div class="inner">' +
                '       <h1>Add new connection</h1>' +
                '       <form onsubmit="return(0)">' +
                '           <input type="text" name="name" placeholder="Connection name">' +
                '           <input type="text" name="hostname" placeholder="Hostname">' +
                '           <input type="text" name="port" placeholder="Port">' +
                '           <input type="password" name="password" placeholder="Password">' +
                '           <button class="primary">Check connection</button>' +
                '       </form>' +
                '   </div>' +
                '</div>'
        }
    }
});